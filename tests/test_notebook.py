import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_jupyter_version(Command):
    cmd = Command('/opt/conda/bin/jupyter --version')
    assert cmd.rc == 0
    assert cmd.stdout.strip() == '4.3.0'


def test_julia_installed(Package):
    julia = Package("julia")
    assert julia.is_installed


def test_environment(Command):
    cmd = Command('source /etc/profile && env')
    env = cmd.stdout
    assert 'EPICS_BASE=/opt/epics/bases/base-3.15.4' in env
    assert 'EPICS_HOST_ARCH=centos7-x86_64' in env


# See https://jira.esss.lu.se/browse/IPYTHON-78
def test_readline(Command):
    assert Command('/opt/conda/bin/python -c "import readline"').rc == 0
    assert Command('/opt/conda/envs/python2/bin/python -c "import readline"').rc == 0
